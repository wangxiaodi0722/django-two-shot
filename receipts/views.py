from django.shortcuts import render
from .models import Receipt

# Create your views here.


def receipts_list(request):
    receipts = Receipt.objects.all()
    context = {
        "receipts": receipts,
    }
    return render(request, "receipts/receipt_list.html", context)

